package psepassignment;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.*;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.swing.*;
import psepassignment.ObjectClasses.*;


public class MainClass extends JFrame{
    
    private static List<Student> studentList;    
    private static List<Teacher> teacherList;    
    private static List<Researcher> researcherList;    
    private static List<Expertise> expertiseList;   
    private static List<Expertise> expertiseListTemp;     
    private static List<Room> roomList;    
    private static List<StudyClass> classList;
    private static List<StudyClass> classListTemp;
    private static List<Person> guestList;
    private static List<Appointment> appointmentList;

    public MainClass()
    {     
        super("University Management System");    
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setExtendedState( getExtendedState()|JFrame.MAXIMIZED_BOTH );
        setMinimumSize(new Dimension(1064, 808));
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(screenSize.width/2-this.getSize().width/2, screenSize.height/2-this.getSize().height/2);
               
        setLayout (new BorderLayout ());
        JPanel outerPanel = new JPanel();
        outerPanel.setLayout(new FlowLayout()); 
        add(outerPanel, BorderLayout.CENTER);
        
        psepassignment.CustomPanels.LoginScreen loginScreen = new psepassignment.CustomPanels.LoginScreen();
        outerPanel.add(loginScreen, BorderLayout.CENTER);
        
        JButton loginButton = loginScreen.getLoginButton();
        JTextField loginText = loginScreen.getUsernameField();
        loginButton.addActionListener( new ActionListener()
                {
                    @Override
                    public void actionPerformed(ActionEvent e)
                    {
                        for (Iterator<Student> i = studentList.iterator(); i.hasNext();) 
                        {
                           Student studentTemp = i.next();
                           if(studentTemp.getUsername().equals(loginText.getText()))
                           {                        
                                outerPanel.removeAll();
                                psepassignment.CustomPanels.StudentScreen studentScreen = new psepassignment.CustomPanels.StudentScreen(studentTemp, teacherList);
                                outerPanel.add(studentScreen, BorderLayout.CENTER);   
                                outerPanel.repaint();
                                outerPanel.revalidate();                                 
                                break;
                           }
                        }
                    }
                });
             
    }
            
    
    public static void main(String[] args) {
        LoadDummyData();
        MainClass app = new MainClass();
        app.setVisible(true);
    }
    
    private static void LoadDummyData()
    {
        //Expertises
        expertiseList = new ArrayList<>();
        expertiseList.add(new Expertise("Database Administration"));
        expertiseList.add(new Expertise("Web Development"));
        expertiseList.add(new Expertise("Network Security"));
        expertiseList.add(new Expertise("Multimedia Design"));
        expertiseList.add(new Expertise("Mobile Development"));
        
        //Rooms
        roomList = new ArrayList<>();
        roomList.add(new Room("Room 413", 30));
        roomList.add(new Room("Room 321", 30));
        roomList.add(new Room("Room 026", 15));
        roomList.add(new Room("1st Amphitheater", 120));
        roomList.add(new Room("2nd Amphitheater", 120));
        roomList.add(new Room("Office 1", 2));
        roomList.add(new Room("Office 2", 2));
        roomList.add(new Room("Office 3", 2));
        roomList.add(new Room("Office 4", 2));
        roomList.add(new Room("Office 5", 2));
        
        //Classes
        classList = new ArrayList<>();
        classList.add(new StudyClass("MSSql Essentials", roomList.get(1), 1, new Time(7,0,0), new Time(9,0,0)));
        classList.add(new StudyClass("Database Security", roomList.get(4), 2, new Time(13,0,0), new Time(15,0,0)));
        classList.add(new StudyClass("Introduction to Angular", roomList.get(2), 3, new Time(11,0,0), new Time(13,0,0)));
        classList.add(new StudyClass("Advanced CSS", roomList.get(3), 4, new Time(17,0,0), new Time(19,0,0)));
        classList.add(new StudyClass("Firewall 101", roomList.get(0), 5, new Time(9,0,0), new Time(11,0,0)));
        classList.add(new StudyClass("Guide on Password Policies", roomList.get(4), 1, new Time(8,0,0), new Time(10,0,0)));
        classList.add(new StudyClass("Photoshop Basics", roomList.get(2), 2, new Time(15,0,0), new Time(17,0,0)));
        classList.add(new StudyClass("Vector Design", roomList.get(1), 3, new Time(10,0,0), new Time(12,0,0)));
        classList.add(new StudyClass("iOS Development", roomList.get(3), 4, new Time(17,30,0), new Time(19,30,0)));
        classList.add(new StudyClass("Developing for Android", roomList.get(0), 5, new Time(8,0,0), new Time(10,0,0)));
        
        //Researchers
        researcherList = new ArrayList<>();
        expertiseListTemp = new ArrayList<>();
        expertiseListTemp.add(expertiseList.get(1));
        researcherList.add(new Researcher("Geoffrey Daniels", 1, "25th Azure Way", "g.daniels@randomuni.com", "gdaniels", roomList.get(5), expertiseListTemp));
        expertiseListTemp = new ArrayList<>();
        expertiseListTemp.add(expertiseList.get(2));
        researcherList.add(new Researcher("Emily Smith", 2, "2nd Brick Road", "e.smith@randomuni.com", "esmith", roomList.get(6), expertiseListTemp));
                
        //Teachers
        teacherList = new ArrayList<>();
        expertiseListTemp = new ArrayList<>();
        expertiseListTemp.add(expertiseList.get(0));
        expertiseListTemp.add(expertiseList.get(1));
        classListTemp = new ArrayList<>();
        classListTemp.add(classList.get(0));
        classListTemp.add(classList.get(1));
        classListTemp.add(classList.get(2));
        classListTemp.add(classList.get(3));
        teacherList.add(new Teacher("Omar Sharif", 3, "387 Bristol Alley", "o.sharif@randomuni.com", "osharif", roomList.get(7), expertiseListTemp, classListTemp, true));
        expertiseListTemp = new ArrayList<>();
        expertiseListTemp.add(expertiseList.get(2));
        classListTemp = new ArrayList<>();
        classListTemp.add(classList.get(4));
        classListTemp.add(classList.get(5));
        teacherList.add(new Teacher("Elena Sokolova", 4, "2nd Yellow Road", "e.sokolov@randomuni.com", "esokolova", roomList.get(8), expertiseListTemp, classListTemp, true));
        expertiseListTemp = new ArrayList<>();
        expertiseListTemp.add(expertiseList.get(3));
        expertiseListTemp.add(expertiseList.get(4));
        classListTemp = new ArrayList<>();
        classListTemp.add(classList.get(6));
        classListTemp.add(classList.get(7));
        classListTemp.add(classList.get(8));
        classListTemp.add(classList.get(9));
        teacherList.add(new Teacher("Andrew Harding", 5, "1st Boulevard", "a.harding@randomuni.com", "aharding", roomList.get(9), expertiseListTemp, classListTemp, true)); 
        
        //One guest
        guestList = new ArrayList<>();
        guestList.add(new Person("Lorenzo Rossi"));
        
        //Appointments
        appointmentList = new ArrayList<>();
        appointmentList.add(new Appointment(guestList.get(0), researcherList.get(1), new Date(2018, 6, 12), new Time(13, 0, 0)));
        appointmentList.add(new Appointment(researcherList.get(0), teacherList.get(2), new Date(2018, 6, 12), new Time(21, 0, 0)));
        
        //Students
        studentList = new ArrayList<>();
        classListTemp = new ArrayList<>();
        classListTemp.add(classList.get(4));
        classListTemp.add(classList.get(5));
        studentList.add(new Student("Grigori Lomtatidze", 1, "53rd Purple Road", "griglom97@gmail.com", "griglomtad", classListTemp));
        classListTemp = new ArrayList<>();
        classListTemp.add(classList.get(0));
        studentList.add(new Student("Amy Beckett", 2, "2nd Blue Alley", "abeckett99@gmail.com", "amybeck99", classListTemp));
        classListTemp = new ArrayList<>();
        studentList.add(new Student("Ernesto Lopez", 3, "34th Pink Alley", "elopez97@yahoo.com", "ernestolop", classListTemp));
        classListTemp = new ArrayList<>();
        classListTemp.add(classList.get(3));
        studentList.add(new Student("Francois Martin", 4, "5th Magenta Street", "f.martin98@gmail.com", "franmartin", classListTemp));
        classListTemp = new ArrayList<>();
        classListTemp.add(classList.get(1));
        classListTemp.add(classList.get(8));
        studentList.add(new Student("Konstantinos Papadopolous", 5, "24th Black Street", "kos.papadop@gmail.com", "kppdpl", classListTemp));
        classListTemp = new ArrayList<>();
        classListTemp.add(classList.get(2));
        studentList.add(new Student("Igor Popov", 6, "72nd Narrow Road", "igor.popov96@gmail.com", "igorpopov", classListTemp));
        classListTemp = new ArrayList<>();
        classListTemp.add(classList.get(7));
        studentList.add(new Student("John Williams", 7, "90th Road", "j.williams2893@gmail.com", "jwilliams12", classListTemp));
        classListTemp = new ArrayList<>();
        studentList.add(new Student("Nicole Grayson", 8, "24th Road", "grayson.nicole82@gmail.com", "n.grayson82", classListTemp));
        classListTemp = new ArrayList<>();
        classListTemp.add(classList.get(8));
        studentList.add(new Student("Hu Qing", 9, "12th Road", "hu.xing2990@gmail.com", "hu.xing", classListTemp));
        classListTemp = new ArrayList<>();
        classListTemp.add(classList.get(0));
        classListTemp.add(classList.get(1));
        classListTemp.add(classList.get(8));
        studentList.add(new Student("Sven Svensson", 10, "42nd Road", "svensson12340@gmail.com", "s.svensson", classListTemp));
        classListTemp = new ArrayList<>();
        studentList.add(new Student("Joseph Camilleri", 11, "53rd Road", "j.camilleri98@hotmail.com", "j.camilleri", classListTemp));
        classListTemp = new ArrayList<>();
        classListTemp.add(classList.get(0));
        classListTemp.add(classList.get(9));
        studentList.add(new Student("Cristina Morrello", 12, "122nd Road", "c.morrello1997@gmail.com", "c.morrello", classListTemp));
        classListTemp = new ArrayList<>();
        studentList.add(new Student("Javier Gomez", 13, "43rd Road", "j.gomez1995@gmail.com", "jav.gomez", classListTemp));
        classListTemp = new ArrayList<>();
        classListTemp.add(classList.get(3));
        classListTemp.add(classList.get(5));
        studentList.add(new Student("Pradesh Patel", 14, "8th Road", "prad.patel4178@gmail.com", "pradesh", classListTemp));
        classListTemp = new ArrayList<>();
        studentList.add(new Student("Khadija Abbas", 15, "4th Road", "k.abbas278@gmail.com", "k.abbas", classListTemp));
    }
    
}
