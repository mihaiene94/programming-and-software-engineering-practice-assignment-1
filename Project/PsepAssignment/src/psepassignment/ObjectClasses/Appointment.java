package psepassignment.ObjectClasses;

import java.sql.Time;
import java.util.Date;

public class Appointment {
    
    private Person FirstParty;
    private Faculty SecondParty;
    private Date AppointmentDate;
    private Time AppointmentTime;
    
    public Appointment(Person inFirstParty, Faculty inSecondParty, Date inAppointmentDate, Time inAppointmentTime)
    {
        FirstParty = inFirstParty;
        SecondParty = inSecondParty;
        AppointmentDate = inAppointmentDate;
        AppointmentTime = inAppointmentTime;
    }
    
    public Person getFirstParty()
    {
        return FirstParty;
    }
    
    public Faculty getSecondParty()
    {
        return SecondParty;
    }
    
    public Date getAppointmentDate()
    {
        return AppointmentDate;
    }
    
    public Time getAppointmentTime()
    {
        return AppointmentTime;
    }
}
