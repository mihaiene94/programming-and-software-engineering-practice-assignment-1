package psepassignment.ObjectClasses;

import java.sql.Time;

public class StudyClass {
    
    private String Name;
    private Room Classroom;
    private int ClassDay;
    private Time ClassStartTime;
    private Time ClassEndTime; 
    
    public StudyClass(String inName, Room inClassroom, int inClassDay, Time inClassStartTime, Time inClassEndTime)
    {
        Name = inName;
        Classroom = inClassroom;
        ClassDay = inClassDay;
        ClassStartTime = inClassStartTime;
        ClassEndTime = inClassEndTime;
    }
          
    public String getName()
    {
        return Name;   
    }
    
    public Room getClassroom()
    {
        return Classroom;   
    }
    
    public int getClassDay()
    {
        return ClassDay;   
    }
    
    public Time getClassStartTime()
    {
        return ClassStartTime;   
    }
    
    public Time getClassEndTime()
    {
        return ClassEndTime;   
    }
    
}
