package psepassignment.ObjectClasses;

import java.util.List;

public class Teacher extends Faculty {
    
    private List<StudyClass> ClassesTaught;
    private Boolean IsProfessor;
    
    public Teacher(String inFullName, int inID, String inAddress, String inEmail, String inUsername, Room inOffice, List<Expertise> inAreaOfExpertise, List<StudyClass> inClassesTaught, Boolean inIsProfessor)
    {    
        super(inFullName, inID, inAddress, inEmail, inUsername, inOffice, inAreaOfExpertise);
        ClassesTaught = inClassesTaught;
        IsProfessor = inIsProfessor;
    }
    
    public List<StudyClass> getClassesTaught()
    {
        return ClassesTaught;
    }
    
    public Boolean isProfessor()
    {
        return IsProfessor;
    }
}
