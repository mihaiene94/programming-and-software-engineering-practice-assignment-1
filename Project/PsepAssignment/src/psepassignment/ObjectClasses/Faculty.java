package psepassignment.ObjectClasses;

import java.util.List;

public class Faculty extends UniversityMember {
    
    private Room Office;
    private List<Expertise> AreaOfExpertise;
    
    public Faculty(String inFullName, int inID, String inAddress, String inEmail, String inUsername, Room inOffice, List<Expertise> inAreaOfExpertise)
    {
        super(inFullName, inID, inAddress, inEmail, inUsername);
        Office = inOffice;
        AreaOfExpertise = inAreaOfExpertise;
    }
    
    public Room getOffice()
    {
        return Office;
    }
    
    public List<Expertise> getAreaOfExpertise()
    {
        return AreaOfExpertise;
    }    
}
