package psepassignment.ObjectClasses;

import java.util.List;

public class Student extends UniversityMember {
    
    public Student(String inFullName, int inID, String inAddress, String inEmail, String inUsername, List<StudyClass> inCurrentClasses)
    {
        super(inFullName, inID, inAddress, inEmail, inUsername);
        CurrentClasses = inCurrentClasses;
    }
    
    private List<StudyClass> CurrentClasses;
    
    public List<StudyClass> getCurrentClasses()
    {
        return CurrentClasses;
    }
    
}
