package psepassignment.ObjectClasses;

public class Room {   
    private String Name;
    private int Capacity;
    
    public Room(String inName, int inCapacity)
    {
        Name = inName;
        Capacity = inCapacity;
    }
    
    public String getName()
    {
        return Name;
    }
    
    public int getCapacity()
    {
        return Capacity;   
    }
    
}
