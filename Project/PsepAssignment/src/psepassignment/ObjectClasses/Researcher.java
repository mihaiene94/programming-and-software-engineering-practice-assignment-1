package psepassignment.ObjectClasses;

import java.util.List;

public class Researcher extends Faculty {
       
    public Researcher(String inFullName, int inID, String inAddress, String inEmail, String inUsername, Room inOffice, List<Expertise> inAreaOfExpertise)
    {
        super(inFullName, inID, inAddress, inEmail, inUsername, inOffice, inAreaOfExpertise);
    }
}
