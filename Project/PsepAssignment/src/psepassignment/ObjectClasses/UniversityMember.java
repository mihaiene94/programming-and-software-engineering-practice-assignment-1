package psepassignment.ObjectClasses;

public class UniversityMember extends Person {
       
    
    private int ID;
    private String Address;
    private String Email;
    private String Username;
            
    public UniversityMember(String inFullName, int inID, String inAddress, String inEmail, String inUsername)
    {
        super(inFullName);
        ID = inID;
        Address = inAddress;
        Email = inEmail;
        Username = inUsername;
    }
    
    public int getID()
    {
        return ID;   
    }
    
    public String getAddress()
    {
        return Address;   
    }
    
    public String getEmail()
    {
        return Email;   
    }
    
    public String getUsername()
    {
        return Username;   
    }
    
}
