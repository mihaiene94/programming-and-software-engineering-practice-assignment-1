package psepassignment.CustomPanels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.*;
import javax.swing.*;

public class LoginScreen extends JPanel {
    
    private JButton btnUserLogin;
    private JTextField txtUserName;
    
    public LoginScreen()
    {
        try
        {            
            setLayout (new BoxLayout(this, BoxLayout.X_AXIS));

            JPanel panUserLogin = new JPanel();
            panUserLogin.setLayout(new GridLayout (3,0,0,10)); 
            
            JLabel lblUserLogin = new JLabel("User Login", JLabel.CENTER);
            lblUserLogin.setFont(new Font(lblUserLogin.getName(), Font.PLAIN, 35));
            panUserLogin.add(lblUserLogin);    
            
            JPanel panUserName = new JPanel();
            panUserName.setLayout (new GridLayout (0,1));
            JLabel lblUserName = new JLabel("Please enter your username", JLabel.LEFT);
            lblUserName.setFont(new Font(lblUserName.getName(), Font.PLAIN, 23));
            panUserName.add(lblUserName);             
            txtUserName = new JTextField();
            txtUserName.setFont(new Font(txtUserName.getName(), Font.PLAIN, 23));
            panUserName.add(txtUserName);   
            panUserLogin.add(panUserName);
            
            btnUserLogin = new JButton("Log in");
            btnUserLogin.setFont(new Font(btnUserLogin.getName(), Font.PLAIN, 23));
            btnUserLogin.setBackground(Color.WHITE);
            panUserLogin.add(btnUserLogin);   
            
            JPanel panGuestLogin = new JPanel();
            panGuestLogin.setLayout(new GridLayout (3,0,0,10)); 
            
            JLabel lblGuestLogin = new JLabel("Guest Login", JLabel.CENTER);
            lblGuestLogin.setFont(new Font(lblGuestLogin.getName(), Font.PLAIN, 35));
            panGuestLogin.add(lblGuestLogin);
            
            JPanel panGuestName = new JPanel();
            panGuestName.setLayout (new GridLayout (0,1));
            JLabel lblGuestName = new JLabel("Please provide a guest name", JLabel.LEFT);
            lblGuestName.setFont(new Font(lblGuestName.getName(), Font.PLAIN, 23));
            panGuestName.add(lblGuestName); 
            JTextField txtGuestName = new JTextField();
            panGuestName.add(txtGuestName);   
            panGuestLogin.add(panGuestName);
            
            JButton btnGuestLogin = new JButton("Log in");
            btnGuestLogin.setFont(new Font(btnGuestLogin.getName(), Font.PLAIN, 23));
            btnGuestLogin.setBackground(Color.WHITE);
            panGuestLogin.add(btnGuestLogin);
            
            Dimension spacing = new Dimension(10,0);
            add(panUserLogin);
            add(new Box.Filler(spacing, spacing, spacing));
            add(new JSeparator(SwingConstants.VERTICAL));
            add(new Box.Filler(spacing, spacing, spacing));
            add(panGuestLogin);
        }
        catch (Exception ex)
        {
            JOptionPane.showMessageDialog(null, "An error has been encountered! We apologize for the inconvenience.", "Error encountered!", JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }
    }
    
    public JButton getLoginButton()
    {
        return btnUserLogin;
    }
    
    public JTextField getUsernameField()
    {
        return txtUserName;
    }
    
}
