package psepassignment.CustomPanels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.*;
import java.util.Iterator;
import java.util.List;
import javax.swing.*;
import psepassignment.ObjectClasses.*;

public class StudentScreen extends JPanel implements ActionListener {
    
    private Student student;
    private List<Teacher> teacherList;
    private JRadioButton rdbSearchByName;
    private JRadioButton rdbSearchByAOE;
    private JTextField txtSearch;
    private JPanel panTeachers;
    private JPanel panCurrentClasses;
    
    public StudentScreen(Student inStudent, List<Teacher> inTeacherList)
    {
        try
        {            
            student = inStudent;
            teacherList = inTeacherList;
            
            Dimension spacing = new Dimension(20,20);
            setLayout (new BoxLayout(this, BoxLayout.Y_AXIS));
            setPreferredSize(new Dimension(1024,768));
                     
            JLabel lblGreeting = new JLabel("Welcome, " + student.getName(), JLabel.CENTER);
            lblGreeting.setFont(new Font(lblGreeting.getName(), Font.PLAIN, 45));
            lblGreeting.setHorizontalAlignment(SwingConstants.CENTER);
            add(lblGreeting);      
            
            add(new Box.Filler(spacing, spacing, spacing));  
            
            JLabel lblCurrentClasses = new JLabel("Current classes", JLabel.CENTER);
            lblCurrentClasses.setFont(new Font(lblCurrentClasses.getName(), Font.PLAIN, 23));
            lblCurrentClasses.setHorizontalAlignment(SwingConstants.LEFT);
            add(lblCurrentClasses);  
            
            panCurrentClasses = new JPanel();
            panCurrentClasses.setLayout (new BoxLayout(panCurrentClasses, BoxLayout.Y_AXIS));
            List<StudyClass> classList = student.getCurrentClasses();
            for (Iterator<StudyClass> i = classList.iterator(); i.hasNext();) 
            {
                StudyClass classTemp = i.next();
                String classDescription = classTemp.getName();
                switch (classTemp.getClassDay()) 
                {
                case 1:  
                    classDescription = classDescription + " - Mondays ";
                    break;
                case 2:  
                    classDescription = classDescription + " - Tuesdays ";
                    break;
                case 3:  
                    classDescription = classDescription + " - Wednesdays ";
                    break;
                case 4:  
                    classDescription = classDescription + " - Thursdays ";
                    break;
                case 5:  
                    classDescription = classDescription + " - Fridays ";
                    break;
                }
                classDescription = classDescription + classTemp.getClassStartTime().toString() + "-" + classTemp.getClassEndTime().toString() + " in " + classTemp.getClassroom().getName();
                JLabel lblClass = new JLabel(classDescription, JLabel.CENTER);
                lblClass.setFont(new Font(lblClass.getName(), Font.PLAIN, 18));
                lblClass.setHorizontalAlignment(SwingConstants.LEFT);
                panCurrentClasses.add(lblClass);  
            }           
            
            JScrollPane scrCurrentClasses = new JScrollPane(panCurrentClasses);
            panCurrentClasses.setAutoscrolls(true);
            scrCurrentClasses.setPreferredSize(new Dimension(800,300));
            this.add(scrCurrentClasses);    
            
            add(new Box.Filler(spacing, spacing, spacing));   
            
            JLabel lblSearch = new JLabel("Search for other classes", JLabel.CENTER);
            lblSearch.setFont(new Font(lblSearch.getName(), Font.PLAIN, 23));
            lblSearch.setHorizontalAlignment(SwingConstants.LEFT);
            add(lblSearch);           
            
            rdbSearchByName = new JRadioButton("Search by teacher name");
            rdbSearchByName.setSelected(true);
            rdbSearchByName.setFont(new Font(rdbSearchByName.getName(), Font.PLAIN, 20));

            rdbSearchByAOE = new JRadioButton("Search by area of expertise");
            rdbSearchByAOE.setFont(new Font(rdbSearchByName.getName(), Font.PLAIN, 20));

            ButtonGroup rdbGroup = new ButtonGroup();
            rdbGroup.add(rdbSearchByName);
            rdbGroup.add(rdbSearchByAOE);
            
            JPanel pnlRDB = new JPanel();
            pnlRDB.setLayout (new GridLayout(4,1,5,0));
            pnlRDB.add(rdbSearchByName);
            pnlRDB.add(rdbSearchByAOE);   
            
            txtSearch = new JTextField();
            txtSearch.setFont(new Font(txtSearch.getName(), Font.PLAIN, 23));
            pnlRDB.add(txtSearch);   
            
            JButton btnSearch = new JButton("Search");
            btnSearch.setFont(new Font(btnSearch.getName(), Font.PLAIN, 23));
            btnSearch.addActionListener(this);
            pnlRDB.add(btnSearch);   
            add(pnlRDB);            
            
            panTeachers = new JPanel();    
            panTeachers.setLayout (new BoxLayout(panTeachers, BoxLayout.Y_AXIS));
            JScrollPane scrTeachers = new JScrollPane(panTeachers);
            panTeachers.setAutoscrolls(true);
            scrTeachers.setPreferredSize(new Dimension(800,600));
            this.add(scrTeachers);            
        }
        catch (Exception ex)
        {
            JOptionPane.showMessageDialog(null, "An error has been encountered! We apologize for the inconvenience.", "Error encountered!", JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }
    }
    
    public void actionPerformed(ActionEvent e) 
    {
        try
        {
            String source = e.getSource().toString();
            panTeachers.removeAll();
            if(rdbSearchByName.isSelected())
            {
                for (Iterator<Teacher> i = teacherList.iterator(); i.hasNext();) 
                {
                    Teacher teacherTemp = i.next();
                    if(teacherTemp.getName().toLowerCase().contains(txtSearch.getText().toLowerCase()))
                    {
                        String teacherDescription = teacherTemp.getName();
                        if(teacherTemp.isProfessor())
                            teacherDescription = teacherDescription + " (Professor)";
                        else
                            teacherDescription = teacherDescription + " (Lecturer)";
                        
                        teacherDescription = teacherDescription + " - Areas of expertise:";
                        List<Expertise> expertiseList = teacherTemp.getAreaOfExpertise();
                        for (Iterator<Expertise> i2 = expertiseList.iterator(); i2.hasNext();) 
                        {
                            teacherDescription = teacherDescription + " " + i2.next().getName();
                        }
                        
                        JLabel lblTeacher = new JLabel(teacherDescription, JLabel.LEFT);
                        lblTeacher.setFont(new Font(lblTeacher.getName(), Font.BOLD, 21));
                        lblTeacher.setHorizontalAlignment(SwingConstants.LEFT);
                        panTeachers.add(lblTeacher); 
                        
                        List<StudyClass> classList = teacherTemp.getClassesTaught();
                        for (Iterator<StudyClass> i3 = classList.iterator(); i3.hasNext();) 
                        {
                            StudyClass classTemp = i3.next(); 
                            String classDescription = classTemp.getName();
                            switch (classTemp.getClassDay()) 
                            {
                            case 1:  
                                classDescription = classDescription + " - Mondays ";
                                break;
                            case 2:  
                                classDescription = classDescription + " - Tuesdays ";
                                break;
                            case 3:  
                                classDescription = classDescription + " - Wednesdays ";
                                break;
                            case 4:  
                                classDescription = classDescription + " - Thursdays ";
                                break;
                            case 5:  
                                classDescription = classDescription + " - Fridays ";
                                break;
                            }
                            classDescription = classDescription + classTemp.getClassStartTime().toString() + "-" + classTemp.getClassEndTime().toString() + " in " + classTemp.getClassroom().getName();
                
                            List<StudyClass> classListStudent = student.getCurrentClasses();
                            JCheckBox chkClassTemp = new JCheckBox(classDescription);
                            chkClassTemp.setFont(new Font(chkClassTemp.getName(), Font.PLAIN, 18));
                            if(classListStudent.contains(classTemp))
                                chkClassTemp.setSelected(true);
                            chkClassTemp.addActionListener( new ActionListener()
                            {
                                @Override
                                public void actionPerformed(ActionEvent e)
                                {
                                    panCurrentClasses.removeAll();
                                    if(chkClassTemp.isSelected())
                                    {
                                        if(!classListStudent.contains(classTemp))
                                        {                                            
                                            classListStudent.add(classTemp);
                                        }
                                    }
                                    else
                                    {
                                        if(classListStudent.contains(classTemp))
                                        {
                                            classListStudent.remove(classTemp);
                                        }                                        
                                    }
                                     for (Iterator<StudyClass> i4 = classListStudent.iterator(); i4.hasNext();) 
                                    {
                                        StudyClass classTemp = i4.next();
                                        String classDescription = classTemp.getName();
                                        switch (classTemp.getClassDay()) 
                                        {
                                        case 1:  
                                            classDescription = classDescription + " - Mondays ";
                                            break;
                                        case 2:  
                                            classDescription = classDescription + " - Tuesdays ";
                                            break;
                                        case 3:  
                                            classDescription = classDescription + " - Wednesdays ";
                                            break;
                                        case 4:  
                                            classDescription = classDescription + " - Thursdays ";
                                            break;
                                        case 5:  
                                            classDescription = classDescription + " - Fridays ";
                                            break;
                                        }
                                        classDescription = classDescription + classTemp.getClassStartTime().toString() + "-" + classTemp.getClassEndTime().toString() + " in " + classTemp.getClassroom().getName();
                                        JLabel lblClass = new JLabel(classDescription, JLabel.CENTER);
                                        lblClass.setFont(new Font(lblClass.getName(), Font.PLAIN, 18));
                                        lblClass.setHorizontalAlignment(SwingConstants.LEFT);
                                        panCurrentClasses.add(lblClass);  
                                    }    
                                panCurrentClasses.repaint();
                                panCurrentClasses.revalidate();       
                                }
                            });
                            panTeachers.add(chkClassTemp);
                        }                                
                    }
                } 
            }
            else                
            {
                for (Iterator<Teacher> i = teacherList.iterator(); i.hasNext();) 
                {
                    Teacher teacherTemp = i.next();
                    List<Expertise> expertiseList = teacherTemp.getAreaOfExpertise();
                    for (Iterator<Expertise> i2 = expertiseList.iterator(); i2.hasNext();) 
                    {                    
                        if(i2.next().getName().toLowerCase().contains(txtSearch.getText().toLowerCase()))
                        {
                        String teacherDescription = teacherTemp.getName();
                        if(teacherTemp.isProfessor())
                            teacherDescription = teacherDescription + " (Professor)";
                        else
                            teacherDescription = teacherDescription + " (Lecturer)";
                        
                        teacherDescription = teacherDescription + " - Areas of expertise:";
                        List<Expertise> expertiseList2 = teacherTemp.getAreaOfExpertise();
                        for (Iterator<Expertise> i3 = expertiseList2.iterator(); i3.hasNext();) 
                        {
                            teacherDescription = teacherDescription + " " + i3.next().getName();
                        }
                        
                        JLabel lblTeacher = new JLabel(teacherDescription, JLabel.LEFT);
                        lblTeacher.setFont(new Font(lblTeacher.getName(), Font.BOLD, 21));
                        lblTeacher.setHorizontalAlignment(SwingConstants.LEFT);
                        panTeachers.add(lblTeacher); 
                        
                        List<StudyClass> classList = teacherTemp.getClassesTaught();
                        for (Iterator<StudyClass> i3 = classList.iterator(); i3.hasNext();) 
                        {
                            StudyClass classTemp = i3.next(); 
                            String classDescription = classTemp.getName();
                            switch (classTemp.getClassDay()) 
                            {
                            case 1:  
                                classDescription = classDescription + " - Mondays ";
                                break;
                            case 2:  
                                classDescription = classDescription + " - Tuesdays ";
                                break;
                            case 3:  
                                classDescription = classDescription + " - Wednesdays ";
                                break;
                            case 4:  
                                classDescription = classDescription + " - Thursdays ";
                                break;
                            case 5:  
                                classDescription = classDescription + " - Fridays ";
                                break;
                            }
                            classDescription = classDescription + classTemp.getClassStartTime().toString() + "-" + classTemp.getClassEndTime().toString() + " in " + classTemp.getClassroom().getName();
                
                            List<StudyClass> classListStudent = student.getCurrentClasses();
                            JCheckBox chkClassTemp = new JCheckBox(classDescription);
                            chkClassTemp.setFont(new Font(chkClassTemp.getName(), Font.PLAIN, 18));
                            if(classListStudent.contains(classTemp))
                                chkClassTemp.setSelected(true);
                            chkClassTemp.addActionListener( new ActionListener()
                            {
                                @Override
                                public void actionPerformed(ActionEvent e)
                                {
                                    panCurrentClasses.removeAll();
                                    if(chkClassTemp.isSelected())
                                    {
                                        if(!classListStudent.contains(classTemp))
                                        {                                            
                                            classListStudent.add(classTemp);
                                        }
                                    }
                                    else
                                    {
                                        if(classListStudent.contains(classTemp))
                                        {
                                            classListStudent.remove(classTemp);
                                        }                                        
                                    }
                                     for (Iterator<StudyClass> i4 = classListStudent.iterator(); i4.hasNext();) 
                                    {
                                        StudyClass classTemp = i4.next();
                                        String classDescription = classTemp.getName();
                                        switch (classTemp.getClassDay()) 
                                        {
                                        case 1:  
                                            classDescription = classDescription + " - Mondays ";
                                            break;
                                        case 2:  
                                            classDescription = classDescription + " - Tuesdays ";
                                            break;
                                        case 3:  
                                            classDescription = classDescription + " - Wednesdays ";
                                            break;
                                        case 4:  
                                            classDescription = classDescription + " - Thursdays ";
                                            break;
                                        case 5:  
                                            classDescription = classDescription + " - Fridays ";
                                            break;
                                        }
                                        classDescription = classDescription + classTemp.getClassStartTime().toString() + "-" + classTemp.getClassEndTime().toString() + " in " + classTemp.getClassroom().getName();
                                        JLabel lblClass = new JLabel(classDescription, JLabel.CENTER);
                                        lblClass.setFont(new Font(lblClass.getName(), Font.PLAIN, 18));
                                        lblClass.setHorizontalAlignment(SwingConstants.LEFT);
                                        panCurrentClasses.add(lblClass);  
                                    }    
                                panCurrentClasses.repaint();
                                panCurrentClasses.revalidate();       
                                }
                            });
                            panTeachers.add(chkClassTemp);
                        }  
                            break;
                        }
                   }

                } 
            }
            panTeachers.repaint();
            panTeachers.revalidate();
        }
        catch (Exception ex)
        {
            JOptionPane.showMessageDialog(null, "An error has been encountered! We apologize for the inconvenience.", "Error encountered!", JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }
    }
    
}
